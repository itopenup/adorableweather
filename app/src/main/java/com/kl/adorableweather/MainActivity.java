package com.kl.adorableweather;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    LocationsPageAdapter mPagesAdapter;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPagesAdapter = new LocationsPageAdapter(getSupportFragmentManager(), this);
        mViewPager = findViewById(R.id.pager);
        mViewPager.setAdapter(mPagesAdapter);
    }
}
