package com.kl.adorableweather;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kl.adorableweather.api.ApiProvider;
import com.kl.adorableweather.api.WeatherApi;
import com.kl.adorableweather.api.models.forecast.ForecastModel;
import com.kl.adorableweather.api.models.weather.WeatherModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherFragment extends Fragment implements OnSuccessListener<Location> {

    @BindView(R.id.txtStatus) TextView mTxtViewStatus;
    @BindView(R.id.txtCurrentTemperature) TextView mTxtViewCurrentTemperature;
    @BindView(R.id.lytErrorContainer) View lytErrorContainer;
    @BindView(R.id.listForecast) RecyclerView mListForecast;

    public static final int LOCATION_TYPE_CURRENT = 0;
    public static final int LOCATION_TYPE_LONDON = 1;
    public static final int LOCATION_TYPE_PARIS = 2;
    public static final int LOCATION_TYPE_NY = 3;
    private static final int PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;

    public static final String TAG = "WeatherFragment";
    public static final String ARG_LOC_TYPE = "type";

    private Context mContext;
    private ForecastListAdapter mForecastListAdapter;


    Callback<WeatherModel> mWeatherCallback = new Callback<WeatherModel>() {
        @Override
        public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
            lytErrorContainer.setVisibility(View.GONE);
            mTxtViewCurrentTemperature.setVisibility(View.VISIBLE);

            WeatherModel weather = response.body();

            if (weather != null) {
                int temp = (int) weather.main.temp;
                mTxtViewCurrentTemperature.setText(getString(R.string.temp_format, temp));
            } else {
                lytErrorContainer.setVisibility(View.VISIBLE);
                mTxtViewStatus.setText("Response error");
                mTxtViewCurrentTemperature.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<WeatherModel> call, Throwable t) {
            lytErrorContainer.setVisibility(View.VISIBLE);
            mTxtViewStatus.setText("Response error");
            mTxtViewCurrentTemperature.setVisibility(View.GONE);
        }
    };

    Callback<ForecastModel> mForecastCallback = new Callback<ForecastModel>() {
        @Override
        public void onResponse(Call<ForecastModel> call, Response<ForecastModel> response) {
            lytErrorContainer.setVisibility(View.GONE);
            mTxtViewCurrentTemperature.setVisibility(View.VISIBLE);

            ForecastModel forecast = response.body();

            if (forecast != null) {
                mForecastListAdapter.setItems(forecast.list);
            } else {
                lytErrorContainer.setVisibility(View.VISIBLE);
                mTxtViewStatus.setText("Response error");
                mTxtViewCurrentTemperature.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<ForecastModel> call, Throwable t) {
            lytErrorContainer.setVisibility(View.VISIBLE);
            mTxtViewStatus.setText("Response error");
            mTxtViewCurrentTemperature.setVisibility(View.GONE);
        }
    };

    public static WeatherFragment newInstance(Context context, int locationType) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARG_LOC_TYPE, locationType);

        final WeatherFragment instance = new WeatherFragment();
        instance.setArguments(arguments);
        instance.mContext = context;

        return instance;
    }

    public static String getTitleByType(Context context, int type) {
        int titleRes = -1;

        switch (type) {
            case LOCATION_TYPE_CURRENT:
                titleRes = R.string.location_type_current;
                break;
            case LOCATION_TYPE_LONDON:
                titleRes = R.string.location_type_london;
                break;
            case LOCATION_TYPE_PARIS:
                titleRes = R.string.location_type_paris;
                break;
            case LOCATION_TYPE_NY:
                titleRes = R.string.location_type_ny;
                break;
        }

        return titleRes == -1
                ? ""
                : context.getResources().getString(titleRes);
    }

    public WeatherFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_location_weather, container, false);

        ButterKnife.bind(this, view);


        mForecastListAdapter = new ForecastListAdapter(mContext);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mListForecast.setLayoutManager(layoutManager);
        mListForecast.setAdapter(mForecastListAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initiateApiRequest();
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {
            ApiProvider
                    .getInstance()
                    .getWeatherApi()
                    .getCurrentByLocation(location.getLatitude(), location.getLongitude())
                    .enqueue(mWeatherCallback);

            ApiProvider
                    .getInstance()
                    .getWeatherApi()
                    .getForecastByLocation(location.getLatitude(), location.getLongitude())
                    .enqueue(mForecastCallback);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationServices
                            .getFusedLocationProviderClient(mContext)
                            .getLastLocation()
                            .addOnSuccessListener(this);
                }
                break;
        }
    }

    @OnClick(R.id.btnRetry)
    public void onRetryClick() {
        initiateApiRequest();
    }

    private void initiateApiRequest() {
        int locationType = 0;

        if (getArguments() != null) {
            locationType = getArguments().getInt(ARG_LOC_TYPE, 0);
        }

        WeatherApi api = ApiProvider
                .getInstance()
                .getWeatherApi();

        switch (locationType) {
            case LOCATION_TYPE_LONDON:
                api.getCurrentByCityName("London,uk").enqueue(mWeatherCallback);
                api.getForecastByCityName("London,uk").enqueue(mForecastCallback);
                break;
            case LOCATION_TYPE_PARIS:
                api.getCurrentByCityName("Paris,fr").enqueue(mWeatherCallback);
                api.getForecastByCityName("Paris,fr").enqueue(mForecastCallback);
                break;
            case LOCATION_TYPE_NY:
                api.getCurrentByCityName("New York,us").enqueue(mWeatherCallback);
                api.getForecastByCityName("Paris,fr").enqueue(mForecastCallback);
                break;
            case LOCATION_TYPE_CURRENT:
                if (ContextCompat.checkSelfPermission(requireActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    lytErrorContainer.setVisibility(View.VISIBLE);
                    mTxtViewStatus.setText("Location permissions required");
                    mTxtViewCurrentTemperature.setVisibility(View.GONE);

                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
                } else {
                    lytErrorContainer.setVisibility(View.GONE);
                    mTxtViewCurrentTemperature.setVisibility(View.VISIBLE);

                    LocationServices
                            .getFusedLocationProviderClient(mContext)
                            .getLastLocation()
                            .addOnSuccessListener(this);
                }
                break;
        }
    }
}
