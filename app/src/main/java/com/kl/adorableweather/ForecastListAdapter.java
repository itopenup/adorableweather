package com.kl.adorableweather;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kl.adorableweather.api.models.forecast.ForecastModel;
import com.kl.adorableweather.api.models.forecast.List;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastListAdapter extends RecyclerView.Adapter<ForecastListAdapter.ViewHolder> {

    private java.util.List<List> mItems;
    private Context mContext;
    private SimpleDateFormat mDateFormat;

    ForecastListAdapter(Context c) {
        mContext = c;
        mDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        mItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.forecast_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position < 0 || position > getItemCount()) {
            return;
        }

        List item = mItems.get(position);

        holder.temp.setText(mContext.getString(
                R.string.temp_format,
                (int)item.main.temp)
        );
        holder.description.setText(item.weather.get(0).description);
        holder.time.setText(mDateFormat.format(new Date(item.dt * 1000L)));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItems(java.util.List<List> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTemp) TextView temp;
        @BindView(R.id.txtDescription) TextView description;
        @BindView(R.id.txtTime) TextView time;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
