package com.kl.adorableweather.api;

import com.kl.adorableweather.api.models.forecast.ForecastModel;
import com.kl.adorableweather.api.models.weather.WeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {
    @GET("weather")
    Call<WeatherModel> getCurrentByCityName(@Query("q") String cityName);

    @GET("weather")
    Call<WeatherModel> getCurrentByLocation(@Query("lat") double lat, @Query("lon") double lon);

    @GET("forecast")
    Call<ForecastModel> getForecastByCityName(@Query("q") String cityName);

    @GET("forecast")
    Call<ForecastModel> getForecastByLocation(@Query("lat") double lat, @Query("lon") double lon);
}
