
package com.kl.adorableweather.api.models.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kl.adorableweather.api.models.Coord;

public class City {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("coord")
    @Expose
    public Coord coord;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("population")
    @Expose
    public int population;

}
