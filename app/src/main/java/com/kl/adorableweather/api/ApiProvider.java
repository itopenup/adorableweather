package com.kl.adorableweather.api;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiProvider {
    private static ApiProvider mProviderInstance;

    private WeatherApi mWeatherApi;

    public static ApiProvider getInstance() {
        if (mProviderInstance == null) {
            mProviderInstance = new ApiProvider();
        }

        return mProviderInstance;
    }

    private ApiProvider() {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("appid", "e484f337d3bb0121763a0a2f0ea18923")
                        .addQueryParameter("units", "metric")
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        mWeatherApi = retrofit.create(WeatherApi.class);
    }

    public WeatherApi getWeatherApi() {
        return mWeatherApi;
    }
}
