package com.kl.adorableweather;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class LocationsPageAdapter extends FragmentPagerAdapter {

    private static final int TABS_COUNT = 4;

    private Context mContext;

    LocationsPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return WeatherFragment.newInstance(mContext, position);
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return WeatherFragment.getTitleByType(mContext, position);
    }
}
